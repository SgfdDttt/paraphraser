export MODEL_PATH=/home/edwardhu/export/paraBank/mono_model_NAACL19
export SCRIPT_PATH=/home/edwardhu/export/paraBank/parabank/scripts/paraphrase_sampler
export REFFILE=`mktemp /tmp/parabank.XXXXXXXXXX` || exit 1

export PYTHONIOENCODING=utf8

. $SCRIPT_PATH/pre.sh \
| \
. $SCRIPT_PATH/translate.sh $1 #\
#| \
#. $SCRIPT_PATH/post.sh
