import sys
sys.path.append('/home/edwardhu/export/paraBank/mono_model_NAACL19_demo')
sys.path.append('/home/edwardhu/export/paraBank/parabank/src')
from argparse import ArgumentParser
import math, random

parser = ArgumentParser(description='Generate OpenFST specifications \
        from nbest list')
parser.add_argument('-n', help='Number of paraphrases per sentence',
        type=int, default=3)
parser.add_argument('-output', help='Root name for output files',
        type=str)
parser.add_argument('-original', help='Original file that was paraphrased',
        type=str)
args = parser.parse_args()

def make_fst(nbest_list):
    symbols=set() # arc label ids
    for sw in nbest_list:
        s,w=sw # weight of sequence and sequence
        symbols.update(set(s.strip(' ').split(' ')))
    symbols.discard('')
    symbols=sorted(list(symbols))
    symbols.insert(0,'<start>'), symbols.insert(0,'<eps>')
    # state ids: 0 is starting state
    state_counter=0
    output=[]
    for sw in nbest_list:
        s,w=sw
        s=s.split(' ') # split sentence into words
        state_counter+=1
        output.append('0 ' + str(state_counter) + ' ' + s[0] + ' ' + s[0])
        for ii,_ in enumerate(s[1:]): 
            state_counter+=1
            output.append(str(state_counter-1) + ' ' \
                    + str(state_counter) + ' ' + s[ii+1] + ' ' + s[ii+1])
        # set last state to be final with weight of sequence. Negative weight so we have logprob
        output.append(str(state_counter) + ' ' + str(-w))
    output='\n'.join(output)
    symbols='\n'.join([s + ' ' + str(ii) for ii,s in enumerate(symbols)])
    return output,symbols

original_sentences=None,None
if args.original is not None:
    original_sentences=[line.strip('\n').split('\t') for line in open(args.original,'r')]
line_counter=0
sentence_counter=0
nbest_list=[]
for line in sys.stdin:
    line_counter+=1
    score=line.strip('\n').split('\t')[0]
    if score=='-inf': # probably empty sentence
        continue
    score=float(score)
    sentence=line.strip('\n').split('\t')[1]
    if sentence=='':
        continue
    nbest_list.append((sentence,score))
    if line_counter % args.n == 0:
        sentence_counter+=1
        nbest_list.append((original_sentences[sentence_counter-1][1],0.)) # prob of 1
        fst,symbols = make_fst(nbest_list)
        filename = args.output
        filename += original_sentences[sentence_counter-1][0] \
                if original_sentences is not None else ''
        filename += '.'+str(sentence_counter)+'.fst'
        with open(filename,'w') as f:
            f.write(fst)
        with open(filename+'.sym','w') as f:
            f.write(symbols)
        nbest_list=[]
        line_counter=0
