training_file='/export/c10/asingh/data/pMNLI/pMNLI/train.h-p'
dev_file='/export/c10/asingh/data/pMNLI/pMNLI/dev_matched.h-p'
test_file='/export/c10/asingh/data/pMNLI/pMNLI/test_matched.h-p'
destination_dir='/export/a12/nholzen/pMNLI_formatted'
labels=['entailment','neutral','contradiction']

def write_data(lines,root_name,label_present=False):
    fr=open(destination_dir+'/'+root_name+'.r','w')
    fl=open(destination_dir+'/'+root_name+'.l','w')
    fq=open(destination_dir+'/'+root_name+'.qrels','w')
    for t in lines:
        fl.write(root_name+'-P'+t[0]+'\t'+t[8]+'\n')
        fr.write(root_name+'-H'+t[0]+'\t'+t[9]+'\n')
        if label_present:
            label_str=t[-1]
            label=str(labels.index(label_str))
        else:
            label='-1'
        fq.write('MNLI-'+root_name+'-P'+t[0]+'\tITER\tMNLI-'+root_name+'-H'\
                    +t[0]+'\t'+label+'\n')
    fr.close(); fl.close(); fq.close()

for filename,splitname in zip([training_file,dev_file,test_file],['train','dev','test']):
    lines=[line.strip('\n').split('\t') for line in open(filename,'r')]
    lines=lines[1:]
    write_data(lines,splitname,label_present='test' not in splitname)
