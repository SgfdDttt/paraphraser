PARAPHRASER_DIR=/home/edwardhu/export/paraBank/mono_model_NAACL19_demo
CONSTRAINTS_MAKER_DIR=/export/a12/nholzen/paraphraser
LIB_PATH=/home/edwardhu/cuda-9.0/lib64
ENV_PATH=/home/edwardhu/parabank-venv

source $ENV_PATH/bin/activate
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIB_PATH

set -x

# RUN PARAPHRASER
CORPUS_FILE=$1 #/export/a12/nholzen/paraphraser/toy_corpus.txt
PARAPHRASE_FILE=$CONSTRAINTS_MAKER_DIR/$(basename $CORPUS_FILE).pp
if [ ! -f $PARAPHRASE_FILE ]; then
    #cd $PARAPHRASER_DIR
    # remove sentence id, run paraphraser, remove BPE
    cat $CORPUS_FILE | cut -f 2- | ./paraphrase2.sh --gpu \
        | sed -r 's/@@( |$)//g' > $PARAPHRASE_FILE
fi

# TURN INTO FST
cd $CONSTRAINTS_MAKER_DIR
FST_DIR=$2
mkdir -p $FST_DIR
n=34 # that's just given by the paraphraser
cat $PARAPHRASE_FILE | python make_fst_spec.py -n $n -output $FST_DIR/ -original $1
for file in $FST_DIR/*.fst
do
    echo $file
    fstcompile --isymbols=$file.sym --osymbols=$file.sym \
        --keep_isymbols --keep_osymbols $file $file.bin || exit 0
    fstdeterminize $file.bin $file.det || exit 0
    fstminimize $file.det $file.min || exit 0
    fstprint $file.min $file.txt || exit 0
    rm $file.bin $file.det $file.min
done
