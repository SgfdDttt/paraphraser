PARAPHRASER_DIR=/home/edwardhu/export/paraBank/mono_model_NAACL19_demo
CONSTRAINTS_MAKER_DIR=/export/a12/nholzen/paraphraser
LIB_PATH=/home/edwardhu/cuda-9.0/lib64
ENV_PATH=/home/edwardhu/parabank-venv

source $ENV_PATH/bin/activate
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIB_PATH

set -x

# RUN PARAPHRASER
CORPUS_FILE=$1
PARAPHRASE_FILE=$CONSTRAINTS_MAKER_DIR/$(basename $CORPUS_FILE).0pp
cat $CORPUS_FILE | cut -f 2- | awk '{print "0.\t"$0}' > $PARAPHRASE_FILE

# TURN INTO FST
cd $CONSTRAINTS_MAKER_DIR
FST_DIR=$2
mkdir -p $FST_DIR
n=1 # not paraphrasing
cat $PARAPHRASE_FILE | python make_fst_spec.py -n $n -output $FST_DIR/ -original $1
ii=1
for file in $FST_DIR/*.fst
do
    echo $file
    fstcompile --isymbols=$file.sym --osymbols=$file.sym \
        --keep_isymbols --keep_osymbols $file $file.bin
    fstdeterminize $file.bin $file.det
    fstminimize $file.det $file.min
    fstprint $file.min $file.txt
    rm $file.bin $file.det $file.min
    ii=$[$ii+1]
done
