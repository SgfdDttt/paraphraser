set -x
datadir=/export/a12/nholzen/pMNLI_formatted
targetdir=/export/a12/nholzen/mnli-paraphrases # where to put the lattices
mkdir -p $targetdir
for x in test dev train
do
    bash run_paraphraser.sh $datadir/$x.l $targetdir
    bash run_paraphraser.sh $datadir/$x.r $targetdir
    cp $datadir/$x.qrels $targetdir
done
