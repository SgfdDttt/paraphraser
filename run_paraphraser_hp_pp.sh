PARAPHRASER_DIR=/home/edwardhu/export/paraBank/mono_model_NAACL19_demo
CONSTRAINTS_MAKER_DIR=/export/a12/nholzen/paraphraser
LIB_PATH=/home/edwardhu/cuda-9.0/lib64
ENV_PATH=/home/edwardhu/parabank-venv

source $ENV_PATH/bin/activate
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIB_PATH

set -x

# 1. DEV
dev_h_file=/export/a12/nholzen/pMNLI_formatted/dev.l
dev_hp_file=/export/a12/nholzen/pMNLI_formatted/dev.hp
dev_p_file=/export/a12/nholzen/pMNLI_formatted/dev.r
dev_pp_file=/export/a12/nholzen/pMNLI_formatted/dev.pp
cd $CONSTRAINTS_MAKER_DIR
FST_DIR=/export/a12/nholzen/pmnli_hp_pp_lattices
mkdir -p $FST_DIR
n=1 # that's just given by the paraphraser
# 1.1. Hypothesis file
cat $dev_hp_file | python make_fst_spec.py -n $n -output $FST_DIR/ -original $dev_h_file
# 1.2. Premise file
cat $dev_pp_file | python make_fst_spec.py -n $n -output $FST_DIR/ -original $dev_p_file
# 2. TEST
test_h_file=/export/a12/nholzen/pMNLI_formatted/test.r
test_hp_file=/export/a12/nholzen/pMNLI_formatted/test.hp
test_p_file=/export/a12/nholzen/pMNLI_formatted/test.l
test_pp_file=/export/a12/nholzen/pMNLI_formatted/test.pp
# 2.1. Hypothesis file
cat $test_hp_file | python make_fst_spec.py -n $n -output $FST_DIR/ -original $test_h_file
# 2.2. Premise file
cat $test_pp_file | python make_fst_spec.py -n $n -output $FST_DIR/ -original $test_p_file
# MAKE FSTS
ii=1
for file in $FST_DIR/*.fst
do
    echo $file
    fstcompile --isymbols=$file.sym --osymbols=$file.sym \
        --keep_isymbols --keep_osymbols $file $file.bin || exit 0
    fstdeterminize $file.bin $file.det || exit 0
    fstminimize $file.det $file.min || exit 0
    fstprint $file.min $file.txt || exit 0
    rm $file.bin $file.det $file.min
    ii=$[$ii+1]
done
