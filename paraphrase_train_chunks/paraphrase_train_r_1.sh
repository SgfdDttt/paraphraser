#!/bin/bash
#$ -j yes
#$ -N pp_train_r_1
#$ -o /export/a12/nholzen/paraphraser/paraphrase_train_chunks/pp_train_r_1.log
#$ -l 'gpu=1,hostname=b1[1-9]*|c*'
set -x
export OMP_NUM_THREADS=1
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
source /home/nholzen/miniconda3/bin/activate
conda activate stog
cd /export/a12/nholzen/paraphraser

chunk_ind=1
ext="r"
datadir=/export/a12/nholzen/pMNLI_formatted
targetdir=/export/a12/nholzen/mnli-paraphrases/train.$ext.$chunk_ind # where to put the lattices
mkdir -p $targetdir; rm -r $targetdir; mkdir $targetdir
x=train
tmp_file=/export/a12/nholzen/mnli-paraphrases/train.$ext.$chunk_ind/tmp.$ext.$chunk_ind.txt
start_ind=$(( (chunk_ind-1)*19636 + 1 ))
stop_ind=$(( chunk_ind*19636 ))
sed -n $start_ind','$stop_ind'p' $datadir/$x.$ext > $tmp_file
bash run_paraphraser.sh $tmp_file $targetdir
rm $tmp_file
