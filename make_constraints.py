import sys
sys.path.append('/home/edwardhu/export/paraBank/mono_model_NAACL19_demo')
sys.path.append('/home/edwardhu/export/paraBank/parabank/src')
from argparse import ArgumentParser
from tf_idf import calcTokenIDF
import math, random

random.seed(24011993)
parser = ArgumentParser(description='Generate set of constraints \
        for each line in input file, to be passed to paraphrase script')
parser.add_argument('-n', help='Number of paraphrases to generate per line',
        type=int, default=3)
args = parser.parse_args()

def fact(nn):
    assert nn>=0
    if nn==0:
        return 1
    else:
        return nn*fact(nn-1)

def sample_poisson(lam,blacklist):
    """ sample from poisson with parameter lam,
    excluding the values in blacklist """
    Z=math.log1p(-sum(math.exp(-lam)*(lam**nn)/fact(nn) for nn in blacklist))
    ii=0
    r=random.random()
    while r>0:
        if ii not in blacklist:
            logfact=sum(math.log(nn) for nn in range(1,ii+1))
            r-=math.exp(-lam + ii*math.log(lam) - logfact - Z)
        ii+=1
    ii-=1
    return ii

def sample_no_replacement(weights,int_sampler,num_samples):
    """ sample num_samples constraints, without replacement,
    each constraint being a binary array of length len(weights)
    with exactly num_bits bits equal to 1 """
    log_weights=[]
    for w in weights:
        if w==0.:
            log_weights.append((-math.inf,0.))
        elif w==1.:
            log_weights.append((0.,-math.inf))
        else:
            log_weights.append((math.log(w),math.log1p(-w)))
    samples,blacklist=[],[0]
    for ii in range(num_samples):
        Z=-math.inf
        while Z==-math.inf: # as long as Z==-math.inf we've completely sampled the space
            num_bits=int_sampler(blacklist) # the number of bits that have to be equal to 1
            if num_bits in blacklist:
                break
            Z=partition_function(num_bits,log_weights)
            for s in samples:
                if sum(s)==num_bits:
                    logprob=[w[0] if b==1 else w[1] \
                            for b,w in zip(s,log_weights)]
                    logprob=sum(logprob)-Z
                    assert logprob<=0., 'logprob: ' + str(logprob)
                    if logprob==0.:
                        Z=-math.inf
                    else:
                        Z+=math.log1p(-math.exp(logprob))
            if Z==-math.inf:
                blacklist.append(num_bits)
                continue
        r=math.log(random.random()) # stay in log space
        for thing in iterate_bits(len(log_weights)):
            if sum(thing)!=num_bits:
                continue
            if thing in samples:
                continue
            assert len(thing)==len(log_weights)
            logprob=[w[0] if b==1 else w[1] \
                    for b,w in zip(thing,log_weights)]
            logprob=sum(logprob)-Z
            if r<logprob:
                samples.append(thing)
                break
            else:
                r+=math.log1p(-math.exp(logprob-r))
    return samples


def iterate_bits(N):
    """ N is the number of bits
    this function  iterates through
    numbers 1 to 2**N in binary format"""
    counter=0
    while counter<2**N:
        tmp=counter
        output=[]
        for ii in range(N):
            output.insert(0,tmp%2)
            tmp//=2
        counter+=1
        yield output

def partition_function(N,log_weights):
    """ log_weights contains log(w_i), log(1-w_i) where
    w_i is the probability for bit i to be equal to 1
    N is the sum of the bits """
    assert N<=len(log_weights)
    k=len(log_weights)
    m={}
    m[(0,0)]=0.
    for ii in range(1,k+1):
        m[(ii,0)]=m[(ii-1,0)]+log_weights[ii-1][1]
    for nn in range(1,N+1):
        m[(0,nn)]=-math.inf
    for ii in range(1,k+1):
        for nn in range(1,N+1):
            a=log_weights[ii-1][1]+m[(ii-1),nn]
            b=log_weights[ii-1][0]+m[(ii-1,nn-1)]
            if a==-math.inf:
                m[(ii,nn)]=b
            elif b==-math.inf:
                m[(ii,nn)]=a
            else:
                m[(ii,nn)]=max(a,b)+math.log1p(math.exp(min(a,b)-max(a,b)))
    return m[(k,N)]

for line in sys.stdin:
    # create set of constraints
    words=line.strip('\n').split(' ')
    weights=[calcTokenIDF(_w) for _w in words]
    n_weights=[w/sum(weights) for w in weights] # weights have to be in [0,1]
    # we can only generate up to 2**num_words constraints
    n_constraints=min(args.n, 2**len(words))
    def int_sampler(blacklist):
        return min(sample_poisson(2.,blacklist),len(words))
    samples=sample_no_replacement(n_weights,int_sampler,n_constraints)
    samples=list(set([tuple(s) for s in samples]))
    while len(samples)<n_constraints: # we need to pad to ensure consistency
        samples.append(samples[-1]) # with subsequent processing modules
    assert len(samples)==n_constraints, 'samples: ' + str(samples) + '\nn_constraints: ' + str(n_constraints)
    constraints=list()
    for s in samples:
        input_to_paraphraser=line.strip('\n')+'\t'
        for ii,b in enumerate(s):
            if b==1:
                input_to_paraphraser+=words[ii]+'|'
        input_to_paraphraser=input_to_paraphraser.strip('|')+'\t'
        constraints.append(input_to_paraphraser)
    sys.stdout.write('\n'.join(sorted(list(constraints)))+'\n')
